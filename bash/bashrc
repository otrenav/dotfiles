#!/usr/bin/env bash

#
# Aliases
#
alias p='cd ~/projects; ls;'
alias d='cd ~/Downloads; ls;'
alias v='cd ~/projects/vodafone/; workon vodafone3.6; ls;'
alias file_count='find . -type f | wc -l'
alias gg='grep --color=auto -rnwi . -e'
alias lsall='ls -flaF --color=auto -lh'
alias ls='ls -lGF --color=auto -lh'
alias grep='grep --color=auto'
alias lss='./local_server.sh'
double_click='xdg-open'
alias t='./test.sh'
alias k='kubectl'
alias g='git'

unamestr=`hostname`
source /usr/local/bin/virtualenvwrapper.sh

#
# R
#
alias R='R --no-save'
alias r='R --no-save'
export R_PROFILE_USER='~/projects/system/dotfiles/r/Rprofile'

#
# Bash-It
#
export BASH_IT=$HOME/.bash_it
export BASH_IT_THEME='otrenav'
if [ -e $BASH_IT/bash_it.sh ]; then
    source $BASH_IT/bash_it.sh
fi
HISTCONTROL=ignoredups:ignorespace

#
# Git
#
source ~/projects/system/dotfiles/git/git-completion.bash

#
# Google Cloud
#
export CLOUDSDK_PYTHON=/usr/bin/python2.7
if [ -e ~/projects/system/gcloud/completion.bash.inc ]; then
    source ~/projects/system/gcloud/path.bash.inc
    source ~/projects/system/gcloud/completion.bash.inc
fi

#
# kubectl
#
source ~/projects/system/dotfiles/bash/kubectl_completion.sh

#
# Editor
#
export VISUAL=vim
export EDITOR="$VISUAL"

#
# MySQL
#
function switch_database() {
    echo "[+] Switching database ($1)..."
    mysql -u root -p -e "DROP DATABASE $1";
    mysql -u root -p -e "CREATE DATABASE $1 CHARACTER SET UTF8;";
    mysql -u root -p $1 < $2
    echo "[+] Done."
}

#
# Python
#
alias kp='killall python'
export WORKON_HOME=~/projects/system/python/

# virtualenvwrapper environments
alias pyclean='find . -name "*.py[co]" -o -name __pycache__ -exec rm -rf {} +'

#
# Fuzzy matching
#
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

#
# Functions
#
function file_directory_size() {
    if du -b /dev/null > /dev/null 2>&1; then
        local arg=-sbh
    else
        local arg=-sh
    fi
    if [[ -n "$@" ]]; then
        du $arg -- "$@"
    else
        du $arg .[^.]* *
    fi
}

function show_keys() {
    gpg $1
    f=$(echo $1 | sed 's/\.[^.]*$//')
    more "${f}"
    rm "${f}"
}

#
# Update path
#
export PATH="$PATH:~/.scripts/"

#
# Node Version Manager (NVM)
#
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
